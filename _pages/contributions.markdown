---
layout: page
title: Contributions
permalink: /contributions/
---

# Contributions

This file list all the contributions that group members have made to Debian Project in the context of `gama.deb` study group.

## QA uploads

| Package | Version | Date     | Reviewed by     | Changed by    |
|---------|---------|----------|-----------------|---------------|
| xshisen | 1.51-6  | 05-10-18 | Lucas Kanashiro | Matheus Faria |
| xsok    | 1.02-18 | 05-10-18 | Lucas Kanashiro | Matheus Faria |

