# Debian Freshman

#### Page

[https://kanashiro.pages.debian.net/gama.deb](https://kanashiro.pages.debian.net/gama.deb)

#### Docker
1. Build the image
```bash
docker build -t freshman .
```

2. Run the image
```bash
docker run -d freshman
```

3. Now browse to http://localhost:4000

#### Installation

1. Install a full [Ruby development environment]()
2. Install Jekyll and [bundler gems]()
```    
gem install jekyll bundler
```
3. Build the site and make it available on a local server
```
bundle exec jekyll serve
```
4. Now browse to http://localhost:4000

