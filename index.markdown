---
layout: page
title: Debian Freshman
---

## Introduction

Debian is a free operating system (OS) for your computer. An operating system is the set of basic programs and utilities that make your computer run. 

Debian provides more than a pure OS: it comes with over 51000 packages, precompiled software bundled up in a nice format for easy installation on your machine.

<!-- {% highlight scss %}
  .header {
    font-size: 100px;
  }
{% endhighlight %} -->

### What is Debian?

> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
